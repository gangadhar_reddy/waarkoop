Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline:
    Given user calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/<product>"
    Then user will verify the status code: <statusCode>

    Examples:
      | product | statusCode |
      | orange  | 200        |
      | mango   | 404        |
      | apple   | 200        |
      | pasta   | 200        |
      | cola    | 200        |