package stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class ProductSteps {


    @When("user calls endpoint {string}")
    public void callEndPoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("user will verify the status code: {int}")
    public void verifyStatusCode(int code) {
        restAssuredThat(response -> response.statusCode(code));
    }
}
